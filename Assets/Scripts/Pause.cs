﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Pause : MonoBehaviour {

    private bool pauseGame = false;
    private bool pauseGui = false;
    public GameObject pauseMenu;




	// Update is called once per frame
	void Update () {
	
        if (Input.GetKeyDown("p"))
        {
            pauseGame = !pauseGame;

            if (pauseGame == true)
            {
                Time.timeScale = 0;
                pauseGame = true;
                pauseGui = true;
                if (pauseGui == true)
                {
                    pauseMenu.SetActive(true);
                }

            }

            if (pauseGame == false)
            {
                Time.timeScale = 1;
                pauseGame = false;
                pauseGui = false;
                pauseMenu.SetActive(false);


            }

        }

        

      

    }
}
