﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EnemySpawner : MonoBehaviour {

	float spawnCD = 0.50f;
	float spawnCDremaining = 10;
    public GameObject winGame;
    public int wave = 0;

    public Text waveText;

    [System.Serializable]
	public class WaveComponent {
		public GameObject enemyPrefab;
		public int num;
		[System.NonSerialized]
		public int spawned = 0;
	}

	public WaveComponent[] waveComps;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		spawnCDremaining -= Time.deltaTime;
		if(spawnCDremaining < 0) {
			spawnCDremaining = spawnCD;

			bool didSpawn = false;

			// spawn wave
			foreach(WaveComponent wc in waveComps) {
				if(wc.spawned < wc.num) {
					wc.spawned++;
					Instantiate(wc.enemyPrefab, this.transform.position, this.transform.rotation);

					didSpawn = true;
					break;
				}
			}

			if(didSpawn == false) {

                //Starts next wave
				if(transform.parent.childCount > 1) {
					transform.parent.GetChild(1).gameObject.SetActive(true);
                    wave += 1;
                    waveText.text = "Wave: " + wave.ToString();

                }
				else {
                   
                    //Win the game 
                    winGame.SetActive(true);
                    Time.timeScale = 0;

                    
                    
				}

				Destroy(gameObject);
			}
		}
	}
}

