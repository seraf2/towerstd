﻿using UnityEngine;
using System.Collections;

public class TowerSpot : MonoBehaviour {



    void OnMouseUp() {
		Debug.Log("TowerSpot clicked.");

		BuildingManager bmanager = GameObject.FindObjectOfType<BuildingManager>();
		if(bmanager.selectedTower != null) {
			ScoreManager smanager = GameObject.FindObjectOfType<ScoreManager>();
			if(smanager.money < bmanager.selectedTower.GetComponent<Tower>().cost) {
				Debug.Log("Not enough money!");
				return;
			} 

			smanager.money -= bmanager.selectedTower.GetComponent<Tower>().cost;

			Instantiate(bmanager.selectedTower, transform.parent.position, transform.parent.rotation);
			Destroy(transform.parent.gameObject);
		}
        else if(bmanager.selectedTower == null)
        {
            
        }



	}

}
