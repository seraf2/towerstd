﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour {

	Transform turretTransform;

	public int cost = 5;
    public int level = 1;
    public float range = 10f;
	public GameObject bulletPrefab;
	
	float fireCooldown = 0.5f;
	float fireCooldownLeft = 0;

	public float damage = 1;
	public float radius = 0;

	// Use this for initialization
	void Start () {
		turretTransform = transform.Find("Turret");
	}
	
	// Update is called once per frame
	void Update () {
		

		Enemy[] enemies = GameObject.FindObjectsOfType<Enemy>();

		Enemy nearestEnemy = null;
		float dist = Mathf.Infinity;

		foreach(Enemy e in enemies) {
			float d = Vector3.Distance(this.transform.position, e.transform.position);
			if(nearestEnemy == null || d < dist) {
				nearestEnemy = e;
				dist = d;
			}
		}

		if(nearestEnemy == null) {
			Debug.Log("No enemies cuz im a bear");
			return;
		}

		Vector3 direction = nearestEnemy.transform.position - this.transform.position;

		Quaternion lookRot = Quaternion.LookRotation( direction );
		turretTransform.rotation = Quaternion.Euler(0, lookRot.eulerAngles.y + 270, 0 );

		fireCooldownLeft -= Time.deltaTime;
		if(fireCooldownLeft <= 0 && direction.magnitude <= range) {
			fireCooldownLeft = fireCooldown;
			ShootAt(nearestEnemy);
		}

        if (level == 2)
        {
            damage *= 2;

        }

        if (level == 3)
        {
            damage *= 2;

        }

    }

	void ShootAt(Enemy e) {
		GameObject bulletGO = (GameObject)Instantiate(bulletPrefab, this.transform.position, this.transform.rotation);

		Bullet b = bulletGO.GetComponent<Bullet>();
		b.target = e.transform;
		b.damage = damage;
		b.radius = radius;
	}

    


}
